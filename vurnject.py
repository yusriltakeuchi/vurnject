import subprocess as sp
import platform
import argparse
import yaml
from httpreq import HTTPReq
from bs4 import BeautifulSoup

'''
    @author: Yusril Rapsanjani
    @code ./yurani
    @site: www.yurani.me

'''

engine = "https://www.bing.com/search?q="
excluded_link = [
    'facebook.com',
    'twitter.com',
    'instagram.com',
    'youtube.com',
    'microsofttranslator.com'
]
vurn_link = []
badlink = []

#Color script
HEADER = '\033[95m'
OKBLUE = '\033[94m'
OKGREEN = '\033[92m'
WARNING = '\033[93m'
FAIL = '\033[91m'

def headers():
    print("{}▒█░░▒█ █░░█ █▀▀█ █▀▀▄ ░░▀ █▀▀ █▀▀ ▀▀█▀▀ ".format(WARNING))
    print("░▒█▒█░ █░░█ █▄▄▀ █░░█ ░░█ █▀▀ █░░ ░░█░░ ")
    print("░░▀▄▀░ ░▀▀▀ ▀░▀▀ ▀░░▀ █▄█ ▀▀▀ ▀▀▀ ░░▀░░ ")
    print ("")
    print(" {}VURNERABILITY INJECTION SCANNER TOOLS".format(WARNING))
    print("      {}Version: v1.0 | ./yurani        ".format(OKGREEN))
    print("       Website: www.yurani.me{}        ".format(WARNING))
    print("________________________________________{}".format(WARNING))

def shellClear():
    system_os = platform.system()
    if 'Linux' in system_os:
        sp.call('clear', shell=True)
    elif "Windows" in system_os:
        sp.call('cls', shell=True)

def Dorkslist(path):
    with open (path, 'r') as dork:
        dorklist = yaml.load(dork)

    return dorklist['Dorks']

def main(dorks, count):
    print("{}[*] Start scanning with dorks {}{}"
        .format(WARNING, OKGREEN, dorks))

    global url
    global excluded_link
    global vurn_link
    global badlink

    #Merge dorks and count
    url = engine
    url += "{}&count={}".format(dorks, count)
    
    #Membuat object HTTPRequest
    httpReq = HTTPReq(url)
    #Mengambil konten source
    content = httpReq.getContent()

    #Memparsing konten tersebut untuk
    #mencari element list li dengan class b_algo
    soup = BeautifulSoup(content, 'html.parser')
    link = soup.findAll("li", class_="b_algo")

    #Looping semua element li yang didapat
    for url in link:
        #Mencari element <a 
        scannedUrl = url.findAll("a", href=True)

        for scan in scannedUrl:
            isNotMatch = False
            for excluded in excluded_link:
                #Mendapatkan target url yang ingin
                #dicek vurnerabilitynya
                target = scan['href']
                #Memastikan bahwa url bukan sesuatu
                #yang ada pada excluded link
                #dan meyakini bahwa url tersebut punya
                #sebuah parameter vurn
                if excluded not in target and "?" in target:
                    isNotMatch = True
                else:
                    isNotMatch = False
            
            if isNotMatch == True and target not in badlink:
                #Traveling ke website untuk mencari vurnerability
                try:
                    print("{}[*] Scanning vurnerability on {}{}{}".format(WARNING, OKGREEN,target, WARNING))

                    httpReq = HTTPReq(target + "'")
                    request = httpReq.getRequest()
                    if "SQL" in request:
                        vurn_link.append(target)

                except Exception as e:
                    print("{}[x] The website is too bad, failed request{}".format(FAIL, WARNING))
                    badlink.append(target)
                    continue


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Vurnject is a tools for scanning vurnerability injection website")
    parser.add_argument('-d', '--dorks', help="Dorks for scanning")
    parser.add_argument('-c', '--count', help="How many item will scanned?")
    args = parser.parse_args()

    if args.dorks and args.count:
        #Membersihkan screen shell
        shellClear()
        #Menampilkan headers
        headers()

        #Start scanning by dork list
        dorkList = Dorkslist(args.dorks)
        for dork in dorkList:
            main(dork, args.count)

        #Menampilkan daftar url yang berhasil
        #ditemukan vurnerability injectionsnya.
        print("{}------- Vurnerability Injection URL -------".format(WARNING))
        for vurn in vurn_link:
            print("{}[Vurn] {}{}".format(WARNING, OKGREEN, vurn))
    else:
        print("Argument to execute tools is wrong.")
        print("Usage: python3 vurnject.py -d <dorkslistPath> -c <count>")